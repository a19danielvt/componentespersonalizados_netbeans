# Simples

## Nuevo proyecto

Abrimos Apache Netbeans y creamos nuestro proyecto. Para ello hacemos *`File > New project...`*. 

Escogemos la carpeta `Java with Ant` y `Java application`.

<div style="text-align:center">
<img src="../.vuepress/public/java_application.png" height="200" width="450">
</div>

Pulsamos **Next** y le ponemos el nombre que queramos y la dirección en la que se guardará. También **desmarcamos** la casilla `Create Main Class`

## Clases

Para este ejemplo solo necesitaremos **dos clases**. La primera será en la que crearemos el componente y la segunda en la que lo provaremos.

En este caso introduciremos estas dos clases en un **paquete** (opcional) que llamaremos ***clases***.

Creamos el paquete pulsando con el botón derecho del ratón en el nombre del proyecto. A continuación `New > Java package...` y le ponemos el nombre que queramos, en este caso *clases*.

Pulsamos botón derecho sobre el paquete (si no tenemos, en el *default package* o en el nombre del proyecto) y creamos la clase del componente `New > Java class...` con nombre ***MyButton***. También creamos la clase en la que provaremos el componente `New > JFrame Form...` con nombre ***Prueba***.

::: warning AVISO
En caso de que no aparezcan estas opciones en la pestaña `New`, pulsamos en `Other` y buscamos lo que necesitemos en el filtro (*Filter*)
:::

Llegados a este punto, la **estructura** de nuestro proyecto debería ser más o menos como la siguiente:

```
Nombre del proyecto
 |--- Source Packages
 |    |--- clases (*default package* por defecto)
 |         |--- MyButton.java
 |         |--- Prueba.java
 |--- Libraries 
```

## Clase del componente

Como ya se comentó en la [introducción](/intro/#como-crear-un-componente), nuestra clase deberá extender de la clase del componente que queremos crear, en este caso [**JButton**](https://docs.oracle.com/javase/7/docs/api/javax/swing/JButton.html), y además debe implementar la clase [**Serializable**](https://docs.oracle.com/javase/7/docs/api/java/io/Serializable.html).

``` java
package clases; // en caso de que creases un paquete

import java.io.Serializable;
import javax.swing.JButton;

public class MyButton extends JButton implements Serializable{

}
```

Ahora mismo, lo único que se falta a nuestro componente para comportarse exactamente igual que un JButton predefinido en Netbeans es un constructor vacío.

``` java
public class MyButton extends JButton implements Serializable{
    
    public MyButton(){

    }

}
```

**Ya tenemos nuestro botón personalizado**. Ahora mismo podríamos [añadir a la paleta](./#anadir-a-la-paleta) este componente y sería igual que un JButton por defecto.

Lo que haremos ahora será añadirle un par de atributos que serán: **color de texto y color de fondo**.

::: tip NOTA
Un botón normal ya trae definidas estas propiedades, pero esto nos servirá de ejemplo para saber cómo añadirlas.
:::

Como nuestros atributos se referirán a cierto color, serán objetos de tipo [*Color*](https://docs.oracle.com/javase/7/docs/api/java/awt/Color.html). Es una buena práctica hacer estos atributos **privados** (aunque no es necesario) ya que se va a necesitar crear sus *Getters y Setters*. 

``` java
import java.awt.Color;

public class MyButton extends JButton implements Serializable{
    
    private Color colorFondo;
    private Color colorTexto;

    public MyButton(){

    }

}
```

Para añadir los *getters* y *setters* de los atributos, con el cursor puesto debajo del constructor, pulsamos con el botón derecho el cualquier zona dentro de la clase y después `Insert Code... > Getter and Setter` y marcamos los atributos de la clase.

``` java
public MyButton(){

}

// el cursor aquí
```

Una vez tenemos los *getters* y *setters* tenemos que **modificar los *setters*** para que, cuando se les dé un valor a cada uno de los atributos, realicen la acción que queremos.

En nuestro caso, cuando se les dé valores a los atributos "colorFondo" y "colorTexo", lo que queremos es que se cambie el color de fondo y el color de texto, respectivamente. Para ello tendremos que llamar dentro de los *setters* a los métodos de la clase JButton que permiten que se realicen estas acciones.

Por lo tanto, los *setters* quedarían de la siguiente manera.

``` java
public void setColorFondo(Color colorFondo) {
    setBackground(colorFondo);
}

public void setColorTexto(Color colorTexto) {
    setForeground(colorTexto);
}
```

::: tip NOTA
Las llamadas a estos métodos, y a cualquiera de los métodos de la clase JButton, son posibles ya que nuestro componente es "hijo" (*extends*) de esa clase.
:::

Ahora sí, nuestro componente está creado y listo para ser usado.:tada:

## Añadir a la paleta

Siempre que queramos **añadir un componente** a la paleta, o **tras modificar la clase** del componente, deberemos realizar los siguiente pasos:

- **Compilar** las clases:
  - Clicamos con el botón derecho sobre el nombre del proyecto y pulsamos `Clean and Build`.
- **Añadir** el componente:
  - Clicamos con el botón derecho sobre la clase del componente y pulsamos `Tools > Add to palette...`
  - Escogemos la categoría en la que lo queremos guardar. Nosotros lo guardaremos en la categoría *Beans*.

<div style="text-align:center">
<img src="../.vuepress/public/anhadido.png" height="75" width="300">
</div>

Si **NO aparece** tu componente en la categoría que indicaste previamente, refresca la paleta pulsando con el click derecho sobre cualquier categoría y a continuación `Refresh Palette`.

::: tip NOTA
Puedes crear tu propia categoría pulsando con el botón derecho sobre una categoría existente y escogiendo `Create New Category`
:::

Solo nos queda arrastrar nuestro componente al JFrame y ver cómo, efectivamente, tiene las propiedades que le dimos (click derecho sobre el componente y `Properties`).

<div style="text-align:center">
<img src="../.vuepress/public/propiedades.png" height="50" width="350">
</div>