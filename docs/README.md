---
home: true
heroImage: /netbeans.png
actionText: Empezar
actionLink: /intro/
footer: Copyright © 2021-present Daniel Vieites
features:
- title: ¿Qué son?
  details: Estos componentes tienen la misma base que los predefinidos por Netbeans pero con funcionalidades propias añadidas por ti.
- title: Rápido
  details: Puedes crear componentes con propiedades sencillas en unos minutos.
- title: Dificultad
  details: Todo depende de lo que necesites y de tu creatividad.
---
