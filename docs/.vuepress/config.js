module.exports = {
    title: "Componentes personalizados",
    description: "Cómo crear componentes personalizados.",
    dest: "public",
    base: "/componentespersonalizados_netbeans/",
    themeConfig: {
        logo: '/netbeans.png',
        nav: [
            {text: 'GitLab', link: 'https://gitlab.com/a19danielvt/componentespersonalizados_netbeans'}
        ],
        sidebar: {
            '/intro/': getSidebar(),
            '/simple/': getSidebar(),
            '/complex/': getSidebar()
        },
        prevLinks: false,
    }
}

function getSidebar(){
    return [
        {
            title: 'Introducción',
            path: '/intro/',
            collapsable: false
        },
        {
            title: 'Atributos',
            collapsable: false,
            children: [
                '/simple/',
                '../complex/'
            ]
        }
    ]
}