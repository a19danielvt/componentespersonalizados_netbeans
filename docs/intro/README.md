# Introducción

Para crear aplicaciones Java en Netbeans con interfaz gráfica (GUI), se suele utilizar el paquete **javax.swing**, que nos proporciona un conjunto de **componentes ligeros** que trabajan de la misma forma en todas las plataformas.

<div style="text-align:center">
<img src="../.vuepress/public/componentes.png" height="300" width="300">
</div>

Estos componentes son los elementos que permiten al usuario interactuar con la aplicación.

## Cómo crear un componente

Cada componente pertenece a una clase en Java, por esta razón, cuando desarrollamos y queremos vincular uno de estos elementos simplemente **instanciamos la clase que necesitamos**, es decir, si queremos un Panel debemos crear un objeto de la clase JPanel.

Además, debe implementar la clase *Serializable* para que Java pueda guardar los datos de nuestro componente.

```java
public class MyButton extends JButton implements Serializable{}
```

A partir de aquí, lo único que nos falta para tener nuestro primer componente personalizado será incluir los atributos que queramos y un constructor vacío para la clase.

Netbeans nos proporciona automáticamente un **panel de modificación para cada atributo** en función de su tipo. Por ejemplo, si el atributo es de tipo Color, aparecerá un panel para escoger el color que deseemos.

<div style="text-align:center">
<img src="../.vuepress/public/paleta_colores.png" height="300" width="500">
</div>

En el caso de que uno de nuestros atributos sea una instancia de una clase creada por nosotros mismos (**atributo complejo**), habrá que crear un **JPanel personalizado** para poder modificar esa propiedad, ya que Netbeans no tendrá un panel de modificación para ese atributo.

## Objetivo

Nuestro objetivo en este tutorial será crear un botón personalizado de dos formas diferentes:

- Con [**atributos simples**](/simple/): esta es la forma más sencilla de crear un componente personalizado.
- Con [**atributos complejos**](/complex/): es decir, con atributos que serán instancias de clases creadas por nosotros.