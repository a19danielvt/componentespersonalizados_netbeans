# Complejos

## Objetivo

Para este ejemplo vamos a partir del ejercicio explicado en el apartado de [atributos simples](/simple/).

En este caso lo que queremos es crear un botón con ciertas propiedades que nos permitan **cambiar el color de fondo y el color de texto al pasar el ratón por encima del componente**.

Es decir, el botón tendrá de inicio unos colores por defecto, al pasar el ratón por encima, cambiarán tanto el color de fondo como el de texto, y al sacar el ratón de encima del botón, los colores volverán a ser los de antes.

## Procedimiento

Será necesario crear:

1. Una [**clase**](./#clase-colores) que guarde los valores de los colores de fondo y texto.
2. Un [**panel personalizado**](./#panel-personalizado) para modificar esos valores.
3. Un [***Listener***](./#listener) que "escuche" las acciones del ratón, que en este caso son entrar y salir del botón.
4. Una clase que nos permita enlazar nuestro panel personalizado con un [**editor de propiedades**](./#editor-de-propiedades).
5. Una clase que extienda de [***SimpleBeanInfo***](./#beaninfo) para enlazar las propiedades en sí con el editor de propiedades, para que, cuando queramos editarlas, nos aparezca el panel que creamos anteriormente.

A medida que vamos creando todo lo mencionado, iremos modificando la clase de nuestro componente.

### Clase "Colores"

Creamos la clase con click derecho sobre el paquete "clases" y `New > Java Class...`.

Nuestro componente va a tener como atributos instancias de esta clase, por lo que también debe **implementar *Serializable***.

Esta clase tendrá dos atributos que serán los mismos que tenemos en este momento en la clase "MyButton".

``` java
package clases;

import java.awt.Color;
import java.io.Serializable;

public class Colores implements Serializable{
    
    private Color colorFondo;
    private Color colorTexto;

}
```

Ahora creamos el constructor, marcando los dos atributos, y los *getters* y *setters* de ambos.

- Click derecho > `Insert Code... > Constructor`.
- Click derecho > `Insert Code... > Getter and Setter`.

``` java
public class Colores implements Serializable{
    
    private Color colorFondo;
    private Color colorTexto;

    public Colores(Color colorFondo, Color colorTexto) {
        this.colorFondo = colorFondo;
        this.colorTexto = colorTexto;
    }

    public Color getColorFondo() {
        return colorFondo;
    }

    public void setColorFondo(Color colorFondo) {
        this.colorFondo = colorFondo;
    }

    public Color getColorTexto() {
        return colorTexto;
    }

    public void setColorTexto(Color colorTexto) {
        this.colorTexto = colorTexto;
    }
}
```

Una vez creada esta clase, podemos modificar nuestro componente y ponerle **dos atributos de tipo *Colores***, que serán "color" y "colorHover". El primero se referirá a los colores que tendrá por defecto el botón y, el segundo, los colores que tendrá cuando el ratón pase por encima.

``` java
public class MyButton extends JButton implements Serializable{
    
    private Colores color;
    private Colores colorHover;
    
    public MyButton(){
        
    }

}
```

Eliminamos los *getters* y *setters* que teníamos antes y creamos otros para los atributos que tenemos ahora.

``` java
public Colores getColor() {
    return color;
}

public void setColor(Colores color) {
    this.color = color;
}

public Colores getColorHover() {
    return colorHover;
}

public void setColorHover(Colores colorHover) {
    this.colorHover = colorHover;
}
```

### Panel personalizado

Este panel será el que nos aparecerá en las propiedades del componente cuando queramos modificar los valores de los colores.

Creamos un nuevo [**JPanel**](https://docs.oracle.com/javase/7/docs/api/javax/swing/JPanel.html) con click derecho sobre el paquete y `New > JPanel Form...`. Le llamaremos "ColoresPanel".

Vamos a la pestaña de `Design` e introducimos en el JPanel un JLabel, un JTextField y un JButton para cada uno de los colores (color de texto y color de fondo). Esto quedaría de la siguiente manera:

<div style="text-align:center">
<img src="../.vuepress/public/panel.png" height="300" width="400">
</div>

Podemos cambiar el nombre de la variable y del texto haciendo click derecho sobre cada uno de los elementos. En nuestro caso cambiamos:

- El texto de los JLabel.
- El texto y los nombres de las variables de los JTextField ("JTextFieldTexto" y "JTextFieldFondo").
- El texto y los nombres de las variables de los JButton ("JButtonTexto" y "JButtonFondo").

Los botones llamarán a una ventana en la que podremos escoger el color que deseemos. Las cajas de texto mostrarán en formato RGB el color elegido.

Nos vamos ahora a la pestaña `Source`.

Ponemos dos atributos de clase, que guardarán el color de fondo y el color del texto.

``` java
public class ColoresPanel extends javax.swing.JPanel {
    
    private Color colorFondo;
    private Color colorTexto;

    public ColoresPanel() {
        initComponents();
    }
    
    // más código
}
    
```

Ahora crearemos un *Listener* para cada uno de los botones. Para ello simplemente tenemos que hacer doble click sobre cada uno de ellos, lo que nos insertará código automáticamente en nuestra clase.

``` java
private void jButtonTextoActionPerformed(java.awt.event.ActionEvent evt) { 
    
}

private void jButtonFondoActionPerformed(java.awt.event.ActionEvent evt) {

}
```

Este código se ejecutará cada vez que pulsemos en dicho botón.

Lo que queremos que se haga cuando se pulse un botón es:

- **Abrir una ventana** que nos permita escoger un color.
- **Guardar ese color** en la variable correspondiente que tenemos como atributo de clase.
- **Mostrar el color** elegido en formato RGB en la caja de texto que le corresponda.

Para crear la ventana que nos permita seleccionar el color que queramos, necesitaremos la clase [**JColorChooser**](https://docs.oracle.com/javase/7/docs/api/javax/swing/JColorChooser.html), la cual tiene un método llamado **showDialog** que nos muestra esa ventana y devuelve el color escogido o null si no se ha escogido ninguno o si se ha cerrado la ventana.

Creamos un método que se llame **getSelectedColor** y que devuelva el objeto **Color** escogido.

``` java
public Color getSelectedColor(){
    return JColorChooser.showDialog(this, "Elige un color", Color.black);
}
```

A este método se le llamará cuando pulsemos un botón, y el color devuelto se deberá guardar en su variable correspondiente.

``` java
private void jButtonTextoActionPerformed(java.awt.event.ActionEvent evt) {                                             
    colorTexto = getSelectedColor(); 
}

private void jButtonFondoActionPerformed(java.awt.event.ActionEvent evt) {                                             
    colorFondo = getSelectedColor();
}
```

Solo nos queda mostrar el color en formato RGB en la caja de texto que le corresponda.

``` java
private void jButtonTextoActionPerformed(java.awt.event.ActionEvent evt) {                                             
    colorTexto = getSelectedColor();
    jTextFieldTexto.setText("[" + colorTexto.getRed() + "," + colorTexto.getGreen() + "," + colorTexto.getBlue() + "]");
}

private void jButtonFondoActionPerformed(java.awt.event.ActionEvent evt) {                                             
    colorFondo = getSelectedColor();
    jTextFieldFondo.setText("[" + colorFondo.getRed() + "," + colorFondo.getGreen() + "," + colorFondo.getBlue() + "]");
}
```

Ahora vamos a crear aquí también un método **getSelectedValue** que nos devuelva un objeto **Colores**. Nos servirá para más adelante.

``` java
public Colores getSelectedValue(){
    if (colorFondo == null) colorFondo = Color.WHITE;
    if (colorTexto == null) colorTexto = Color.BLACK;
    return new Colores(colorFondo, colorTexto);
}
```

::: tip NOTA
Le damos colores por defecto blanco y negro para el fondo y el texto respectivamente para que no devuelva nunca un valor nulo.
:::

### *Listener*

Esta clase será una **interfaz** con un único método **changeColorWhenHover** que devolverá un *boolean*.

El usuario que utilice nuestro componente deberá sobreescribir este método y devolver "**true**" para "activar" la funcionalidad de cambiar el color cuando el ratón pase por encima del botón.

Para crearla hacemos click derecho sobre el paquete de clases y `New > Java Interface...` y le ponemos de nombre **ColorHoverListener**:

``` java
public interface ColorHoverListener {
    public boolean changeColorWhenHover();
}
```

Ahora tenemos que añadir a la clase de nuestro componente un atributo más que de tipo ColorHoverListener.

``` java
private Colores color;
private Colores colorHover;
private ColorHoverListener listener;
```

Y los métodos propios de los *listeners* que son los de añadir y eliminar el *listener*.

``` java
public void addColorHoverListener(ColorHoverListener listener){
    this.listener = listener;
}

public void removeColorHoverListener(){
    this.listener = null;
}
```

Lo siguiente que haremos será el código necesario para que, cuando el ratón entre en el componente, cambie de color y que, cuando salga del componente, vuelvan a ponerse los colores que estaban anteriormente.

Esto se conseguirá sobreescribiendo los *listeners* que permiten escuchar las acciones del ratón. Se debe poner en el constructor del componente.

Además, vamos a crear un método que se encargue de cambiar los colores del componente.

``` java
public MyButton() {
    /**
    * Pone unos colores por defecto al botón, ya que de no ser 
    * así no funcionaría correctamente.
    */
    color = new Colores(Color.WHITE, Color.BLACK);
    changeColors(color);
    
    this.addMouseListener(new MouseAdapter(){
        
        /**
         * Si el usuario ha añadido el listener, el método del listener
         * devuelve true y se han definido los colores que tendrá
         * el botón cuando el ratón pase por encima... Entonces se
         * cambiarán los colores del componente.
         */
        @Override
        public void mouseEntered(MouseEvent e){
            if (listener != null && listener.changeColorWhenHover() &&
                    colorHover != null){
                        
                changeColors(colorHover);
            }
        }
        
        /**
         * Igual que antes, pero ahora es el color por defecto el que 
         * no puede ser null.
         */
        @Override
        public void mouseExited(MouseEvent e){
            if (listener != null && listener.changeColorWhenHover() && 
                    color != null){
                
                changeColors(color);
            }
        }
    });
}

// método que se encarga de cambiar los colores del componente
public void changeColors(Colores colors){
    if(colors.getColorFondo() != null)
        setBackground(colors.getColorFondo());

    if(colors.getColorTexto() != null)
        setForeground(colors.getColorTexto());
}
```

### Editor de propiedades

Esta clase será la encargada de enlazar nuestro panel personalizado con un editor de propiedades de java.

Para eso, la clase debe extender de [**PropertyEditorSupport**](https://docs.oracle.com/javase/7/docs/api/java/beans/PropertyEditorSupport.html) y tendremos que sobreescribir algunos métodos.

Para crearla hacemos click derecho sobre el paquete de clases y `New > Java Class...` y le llamamos **ColoresPropertyEditorSupport**.

Esta clase tendrá un atributo que será una instancia del paner que hemos creado.

``` java
public class ColoresPropertyEditorSupport extends PropertyEditorSupport{

    private ColoresPanel panel = new ColoresPanel();

}
```

Ahora sobreescribiremos los siguientes métodos pulsando con el click derecho en la clase y a continuación `Insert Code... > Override Method...`.

- **supportsCustomEditor**
- **getCustomEditor**
- **getJavaInitializationString**
- **getValue**

Los métodos quedarían de la siguiente manera:

``` java
/**
 * Devuelve true para que nos permita utilizar nuestro panel.
 */
@Override
public boolean supportsCustomEditor() {
    return true;
}

/**
 * Devuelve el editor que vamos a utilizar, es decir, nuestro panel.
 */
@Override
public Component getCustomEditor() {
    return panel;
}

/**
 * Devuelve un String que simula la creación de un objeto Colores, es decir, 
 * lo que se crearía tras editar la propiedad en el panel.
 *
 * En este y en el siguiente método utilizamos el método getSelectedValue 
 * de la clase del panel.
 *
 * La clase Color tiene como uno de sus constructores el siguiente: 
 * Color(red, green, blue);
 */
@Override
public String getJavaInitializationString() {
    Colores c = panel.getSelectedValue();
    Color fondo = c.getColorFondo();
    Color texto = c.getColorTexto();

    return "new Colores(new java.awt.Color(" + fondo.getRed() + ", " + 
            fondo.getGreen() + ", " + fondo.getBlue() + "), " + 
            "new java.awt.Color(" + texto.getRed() + ", " + texto.getGreen() +
            ", " + texto.getBlue() + "))";
}

/**
 * Igual que en el método anterior, devuelve el objeto que se crearía tras
 * editar la propiedad en el panel.
 */
@Override
public Object getValue() {
    return panel.getSelectedValue();
}
```

### BeanInfo

Ahora crearemos la clase que nos permitirá **enlazar el componente con el panel**, para que, cuando queramos editar las propiedades del botón, nos aparezaca el panel personalizado.

Para ello, simplemente tendremos que hacer click derecho sobre la clase del componente, pulsar en `BeanInfo Editor...` y decirle que SÍ que queremos crearlo.

Esto nos creará una clase con el **nombre de la clase de nuestro componente** + **BeanInfo**, con un montón de variables estáticas y propiedades.

Ahora nos tenemos que mover a la pestaña de ***Designer*** y buscar las propiedades que le dimos a nuestro componente: **color** y **colorHover**.

::: warning ATENCIÓN
Si aparecen todas las **variables estáticas** y las **propiedades** en **gris**, hay que activarlas. Esto se hace yendo a cada una de las pestañas en el apartado "Designer" (Bean, Properties, Event Sources y Methods) y desmarcando la casilla "**Get From Introspection**".
:::

Una vez encontramos nuestras propiedades (en la pestaña `Properties` dentro de `Designer`), tendremos que cambiar en cada una de ellas el valor de la casilla "**Property Editor Class**".

Lo que tenemos que escribir aquí es la ruta desde la carpeta `Source Packages` hasta la clase ColoresPropertyEditorSupport. En nuestro caso tendremos que escribir lo siguiente:

``` java
// Si la clase está en un paquete:

    <nombre del paquete>.<nombre de la clase>.class
    clases.ColoresPropertyEditorSupport.class

// Si la clase está en el paquete por defecto:

    <nombre de la clase>.class
    ColoresPropertyEditorSupport.class
```

::: warning ATENCIÓN
Es muy importante que esté bien escrito, ya que de no ser así no funcionará, pero tampoco dará ningún tipo de error.
:::

## Últimos pasos

Lo que nos falta por hacer es [**añadir a la paleta**](../simple/#anadir-a-la-paleta) el componente y **probarlo**.

Hay que recordar que para probar la funcionalidad de nuestro componente, el usuario debe añadir el listener que creamos. Ahora veremos cómo.

Una vez añadido el componente a la paleta, **arrastramos nuestro botón** al JFrame y lo primero que deberíamos de ver son los colores que le pusimos por defecto al botón.

``` java
public MyButton() {
    /**
    * Pone unos colores por defecto al botón, ya que de no ser 
    * así no funcionaría correctamente.
    */
    color = new Colores(Color.WHITE, Color.BLACK);
    changeColors(color);
    
    // más código
}
```

<div style="text-align:center">
<img src="../.vuepress/public/colores_defecto.png" height="250" width="300">
</div>

Ahora vamos a cambiarle las propiedades. Click derecho sobre el componente y `Properties`. Vamos a cambiar los colores de la pestaña "colorHover".

::: tip NOTA
Si **no** te aparece el panel personalizado, revisa los pasos anteriores. Una vez modificado lo necesario, vete al apartado de añadir a la paleta.
:::

Una vez cambiados los colores de fondo y texto, nos falta **añadir el ColorHoverListener**.

Esto se hace en la clase Prueba ya que, como dijimos antes, es el usuario el que debe realizar esta acción si quiere utilizar esta funcionalidad.

``` java
public Prueba() {
    initComponents();

    myButton1.addColorHoverListener(new ColorHoverListener() {
        @Override
        public boolean changeColorWhenHover() {
            return true;
        }
    });
}
```

Ahora sí. **Es el momento de probarlo**. :tada: 